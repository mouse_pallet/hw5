package tep;

import java.io.*;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.BufferedReader;
import java.util.StringTokenizer;
//import java.io.IOException;
import java.util.ArrayList;

public class Main {
	static float[][] point =new float[5][2];
	//static float[][] point =new float[8][2];
	//static float[][] point =new float[16][2];
	//static float[][] point =new float[24][2];
	//static float[][] point =new float[128][2];
	//static float[][] point =new float[512][2];
	//static float[][] point =new float[2048][2];
	static ArrayList<Integer> turn_list = new ArrayList<Integer>();
	
	public static void main(String[] args) {
		
        try {
            //ファイルを読み込む
        	FileReader fr = new FileReader("/home/step/google-2014-step-tsp/input_0.csv");
            BufferedReader br = new BufferedReader(fr);

            //読み込んだファイルを１行ずつ処理する
            String line;
            StringTokenizer token;
            
            //**********一行飛ばすために、一回から回す
           //区切り文字","で分割する
            if((line = br.readLine()) != null) {
            token = new StringTokenizer(line, ",");

            //分割した文字を画面出力する
            while (token.hasMoreTokens()) {
            	token.nextToken();
               // System.out.println(token.nextToken());
            }
                     
            }
            //*************************
            //LineNumberReader fin=new LineNumberReader(fr);
            //float[][] point =new float[fin.getLineNumber()][2];
            //System.out.println(fin.getLineNumber());
            
            int i=0;
            int j=0;
            for(i=0;(line = br.readLine()) != null;i++){
 
                //区切り文字","で分割する
                token = new StringTokenizer(line, ",");

                //分割した文字を画面出力する
                for(j=0;token.hasMoreTokens();j++) {
                	
                point[i][j]=Float.parseFloat(token.nextToken());
                	//System.out.println(point[i][j]);
                 }
            }


           tsp(point);
            //NAM.NAM(point);
            //終了処理
            br.close();

        } catch (IOException ex) {
            //例外発生時処理
            ex.printStackTrace();
        }
		
	}
	
	
	//順番通り0123...
	public static void tsp(float[][] point){
		float distant=0;
		float dx,dy;
		
		
		
		for(int i=0;i<point.length;i++){
			if((i+1)<point.length){
				dx=point[i][0]-point[i+1][0];
				dy=point[i][1]-point[i+1][1];
				distant+=Math.sqrt(dx*dx+dy*dy);
			}
			else{//0に戻る
				dx=point[i][0]-point[0][0];
				dy=point[i][1]-point[0][1];
				distant+=Math.sqrt(dx*dx+dy*dy);
			}
		}
		System.out.println("D="+distant);
	}
}

Yuko Yanagawa
Yuko Yanagawa
