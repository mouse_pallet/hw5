package tep;


import java.util.LinkedList;


//最近追加法
public class NAM {
	
	static LinkedList<Integer> turn_list = new LinkedList<Integer>();//経路
	
	static float gx=0;
	static float gy=0;//重心位置
	
	/*pointはオブジェクトでよかったかもしれないです*/
	public static void NAM(float[][] point){
		Start(point);//初期回路を作る
		CenterG(point);//回路の重心を求める
		int nextp;
		nextp=Gi(point);//重心から最も近い回路外の点を見つける
		while(nextp>0){
			NearP(point,nextp);//点を回路に加える
			CenterG(point);//回路の重心を求める
			nextp=Gi(point);//重心から最も近い回路外の点を見つける
		}
		turn_list.add(0);//start地点を最後尾に代入
		
		float dx;
		float dy;
		float D=0;
		for(int j=0;j<turn_list.size()-1;j++){
			dx=point[turn_list.get(j)][0]-point[turn_list.get(j+1)][0];
			dy=point[turn_list.get(j)][1]-point[turn_list.get(j+1)][1];
			D+=Math.sqrt(dx*dx+dy*dy);
		}
		System.out.println("distance:"+D);
		
	}
	
	public static void Start(float[][] point){
		float distant=0;
		float dx,dy;
		float minf,mins;
		float flbox;
		int inbox;
		int first,second;
		
		//始点の設定
		turn_list.add(0);
		
		//初期回路を作る(始点から最も近い2つの点をえらぶ)
		
		dx=point[0][0]-point[1][0];
		dy=point[0][1]-point[1][1];
		minf=dx*dx+dy*dy;
		first=1;
		
		dx=point[0][0]-point[2][0];
		dy=point[0][1]-point[2][1];
		mins=dx*dx+dy*dy;
		second=2;
		
		/*この部分関数化したいけど複数の戻り値ができなくて諦めました。*/
		if(minf>mins){
			//swap(minf,mins);
	        flbox= mins;
	        mins= minf;
	        minf =flbox;
	        
	        inbox=first;
	        first=second;
	        second=inbox;
		}
		
		for(int i=2;i<point.length;i++){
			dx=point[0][0]-point[i][0];
			dy=point[0][1]-point[i][1];
			distant=dx*dx+dy*dy;
			if(distant<mins){
				if(distant<minf){
					second=first;
					mins=minf;
					
					first=i;
					minf=distant;
				}
				else{
					second=i;
					mins=distant;
				}
			}
		}
		
		turn_list.add(first);
		turn_list.add(second);
		//System.out.println("start_list="+turn_list);
	}
	
	
		//出来た回路の重心gを求める
	public static void CenterG(float[][] point){  	
		gx=0;
		gy=0;
		
		for(int i=0;i<turn_list.size();i++){
			gx=gx+point[turn_list.get(i)][0];
			gy=gy+point[turn_list.get(i)][1];
		}
		gx=gx/turn_list.size();
		gy=gy/turn_list.size();
	}
	
	//回路外で、Gと最も近しい点を求める
	public static int Gi(float[][] point){
		float dgx,dgy;
		float distant;
		float min=Float.MAX_VALUE;
		int first=-1;
		for(int i=1;i<point.length;i++){
			if(turn_list.contains(i)==false){
				   //System.out.println("i="+i);
				   dgx=gx-point[i][0];
	               dgy=gy-point[i][1];
	               distant=dgx*dgx+dgy*dgy;
	               if(min>distant){
	            	   min=distant;
	            	   first=i;
	               }
	          }
		}
		
		return first;
	} 
	
	//最も近い1点を見つけて、その隣に挿入
	public static void NearP(float[][] point,int k){
		float dx,dy;
		float distant;
		float dleft,dright;
		float min=Float.MAX_VALUE;
		int first=-1;
		for(int i=0;i<turn_list.size();i++){
	               dx=point[k][0]-point[turn_list.get(i)][0];
	               dy=point[k][1]-point[turn_list.get(i)][1];
	               distant=dx*dx+dy*dy;
	               if(min>distant){
	            	   min=distant;
	            	   first=i;
	               }
	     }
		
		if(first==0){//start地点が最も近いなら
			turn_list.add(first+1, k);
		}
		else if(first==turn_list.size()-1){//一番端の地点が最も近いなら
			turn_list.add(first);
		}
		else{
		//左右どちらか、距離が近いほうの間に挿入
		dx=point[k][0]-point[turn_list.get(first-1)][0];
		dy=point[k][1]-point[turn_list.get(first-1)][1];
    	dleft=dx*dx+dy*dy;
	
    	dx=point[k][0]-point[turn_list.get(first+1)][0];
		dy=point[k][1]-point[turn_list.get(first+1)][1];
    	dright=dx*dx+dy*dy;
    	
    	if(dright<dleft)turn_list.add(first+1, k);
    	else turn_list.add(first, k);
		}
	}
	
}

Yuko Yanagawa
